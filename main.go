package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"sync"
)

type Story struct {
	Title string `json:"title"`
	Url   string `json:"url"`
	Score int32  `json:"score"`
	By    string `json:"by"`
}

const storiesUrl = "https://hacker-news.firebaseio.com/v0/topstories.json"
const itemUrlBase = "https://hacker-news.firebaseio.com/v0/item"

func main() {
	res, err := http.Get(storiesUrl)
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}
	ids := []int32{}
	err = json.Unmarshal(body, &ids)
	if err != nil {
		panic(err)
	}

	topFive := ids[0:5]

	var wg sync.WaitGroup
	for _, id := range topFive {
		wg.Add(1)
		go GetStory(id, &wg)
	}
	wg.Wait()
}

func GetStory(id int32, wg *sync.WaitGroup) {
	res, err := http.Get(fmt.Sprintf("%s/%d.json", itemUrlBase, id))
	if err != nil {
		panic(err)
	}
	defer res.Body.Close()

	body, err := ioutil.ReadAll(res.Body)
	if err != nil {
		panic(err)
	}

	story := Story{}
	err = json.Unmarshal(body, &story)
	if err != nil {
		panic(err)
	}
	fmt.Printf("%s\nScore: %d\nBy: %s\nURL: %s\n\n", story.Title, story.Score, story.By, story.Url)

	wg.Done()
}
